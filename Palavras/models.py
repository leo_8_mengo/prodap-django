from django.db import models
from Categorias.models import Categoria  

# Create your models here.
class Palavra(models.Model):
    nome = models.CharField(max_length=150)
    categoria = models.ForeignKey(Categoria, on_delete=models.DO_NOTHING)
    video = models.CharField(max_length=250)
    descricao = models.CharField(max_length=250)
    sw =models.ImageField(upload_to="Palavras", null=True, blank=True)
    
    def __str__(self):
        return self.nome    