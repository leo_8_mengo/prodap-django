function get_palavra(categoria){
    token = document.getElementsByName("csrfmiddlewaretoken")[0].value;

    $.ajax({
        type: 'POST',
        url: '/get_palavras/' + categoria + '/',
        data: {
            csrfmiddlewaretoken: token,
        },
        success: function(result){
            var pal_encontradas = document.getElementById("pal_encontradas");
            pal_encontradas.innerHTML = ""
            var palavra = result["palavras"]
            for(pal in palavra){
                pal_encontradas.innerHTML += "<div onclick=\"get_atributos("+palavra[pal]["id"]+")\" style=\"cursor: pointer;\">"
                 +palavra[pal]["nome"] 
                 +"<\div>";
            }
            
        }
    });
}

function get_atributos(id){
    token = document.getElementsByName("csrfmiddlewaretoken")[0].value;

    $.ajax({
        type: 'POST',
        url: '/get_atributos/' + id + '/',
        data: {
            csrfmiddlewaretoken: token,
        },
        success: function(result){
            var pal_atributos = document.getElementById("pal_atributos");
            console.log(result["atributos"]);
            var atributos = result["atributos"];
            pal_atributos.innerHTML = "";
            pal_atributos.innerHTML += "<div class='col-sm-8 embed-responsive embed-responsive-16by9'>"
                                    +"<iframe embed-responsive-item "
                                    +"src='https://www.youtube.com/embed/"+ atributos[0]["video"] +"?autoplay=1'"
                                    +"</iframe>"
                                    +"</div>";
             
            pal_atributos.innerHTML += "<div class='col-sm-5'>"
                                    +"<img class='img-fluid'"
                                    +"src=\"media/"+atributos[0]["signwrite"]+"\""
                                    +">"
                                    +"</div>";

            pal_atributos.innerHTML += "<div class='col-sm-5'><textarea readonly>"
                                    +atributos[0]["descricao"]
                                    +"</textarea></div>";
            
            
            // var palavra = result["palavras"]
            // for(pal in palavra){
            //     pal_encontradas.innerHTML += "<div onclick=\"get_atributos("+palavra[pal]["id"]+")\" style=\"cursor: pointer;\">"
            //      +palavra[pal]["nome"] 
            //      +"<\div>";
            // }
            
        }
    });
}