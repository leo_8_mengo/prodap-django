from django.urls import path

from Categorias.views import HomeView, get_palavras, get_atributos

urlpatterns = [
    path('', HomeView.as_view(), name='article-list'),
    path('get_palavras/<int:pk>/', get_palavras.as_view(), name='get_palavras'),
    path('get_atributos/<int:pk>/', get_atributos.as_view(), name='get_atributos'),
]