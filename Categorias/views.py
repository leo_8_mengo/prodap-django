from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.base import View
from django import template
from django.http import HttpResponse
from django.core import serializers
from django.http import JsonResponse
import json


from Categorias.models import Categoria
from Palavras.models import Palavra
from Atributos.models import Atributos

class HomeView(ListView):

    model = Categoria
    template_name = "home/home.html"
    register = template.Library()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['palavras'] = Palavra.objects.all()
        return context


class get_palavras(View):
    def post(self,request, pk):
        palavras = Palavra.objects.filter(categoria=pk).values('id','nome')
        return JsonResponse({'palavras': list(palavras)}) 

class get_atributos(View):
    def post(self, request, pk):
        atributos = Atributos.objects.filter(palavra=pk).values('video','signwrite','descricao')
        return JsonResponse({'atributos': list(atributos)}) 