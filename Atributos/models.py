from django.db import models
from Palavras.models import Palavra

class Atributos(models.Model):
    palavra = models.ForeignKey(Palavra, on_delete=models.DO_NOTHING)
    video = models.CharField(max_length=250)
    signwrite = models.ImageField(upload_to="Atributos", null=True, blank=True)
    descricao = models.CharField(max_length=250)

    def __str__(self):
        return self.palavra.nome 