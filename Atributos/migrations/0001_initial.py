# Generated by Django 2.1.7 on 2019-04-21 21:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('Palavras', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Atributos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('video', models.CharField(max_length=250)),
                ('signwrite', models.ImageField(blank=True, null=True, upload_to='Atributos')),
                ('descricao', models.CharField(max_length=250)),
                ('palavra', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='Palavras.Palavra')),
            ],
        ),
    ]
